﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MCC
{
    class DichotomyMethod
    {
        public Func<double, double, double, double> DichotomyFunc { get; set; }

        public Results Start(double boundLeft, double boundRight, double x, double y, double epsilon = 0.001)
        {
            double x1 = 0.0;
            double x2 = 0.0;
            double f1 = 0.0;
            double f2 = 0.0;
            var halfEps = epsilon / 2;

            List<Results> x1Res = new List<Results>();
            List<Results> x2Res = new List<Results>();
            for (int i = 0; ; i++)
            {
                x1 =  (boundLeft + boundRight - halfEps) / 2;
                x2 =  (boundLeft + boundRight + halfEps) / 2;

                f1 = DichotomyFunc(x, y, x1);
                f2 = DichotomyFunc(x, y, x2);

                if (f1 <= f2)
                {
                    boundRight = x2;
                }
                else
                {
                    boundLeft = x1;
                }

                x1Res.Add(new Results { Iteration = i, X = x1, F = f1 });
                x2Res.Add(new Results { Iteration = i, X = x2, F = f2 });

                if (boundRight - boundLeft <= epsilon)
                {
                    break;
                }
            }

            var root = FindLowestRoot(x1Res, x2Res);
            return root;
        }

        Results FindLowestRoot(List<Results> x1Res, List<Results> x2Res)
        {
            var min1 = x1Res.Min();
            var min2 = x2Res.Min();

            if (min1.F > min2.F)
                return min1;

            return min2;
        }
    }

    public class Results : IComparable
    {
        public int Iteration { get; set; }
        public double X { get; set; }
        public double F { get; set; }

        public int CompareTo(object obj)
        {
            var compObj = obj as Results;

            if (this.F < compObj.F) 
            { 
                return -1;
            }

            if (Math.Round(this.F, 10) == Math.Round(compObj.F, 10)) 
            { 
                return 0;
            }

            return 1;
        }
    }
}
