﻿using System;

namespace MCC
{
    public class GreatDescent
    {
        // Функция Розенброка
        double Func(double x1, double x2)
        {
            return Math.Pow((1 - x1), 2) + 100 * Math.Pow((x2 - Math.Pow(x1, 2)), 2);
        }

        //Первая производная по dx
        double Func_Dx(double x1, double x2)
        {
            return 200 * x1 * (Math.Pow(x1, 2) - x2) + x1 - 1;
        }

        //Первая производная по dy
        double Func_Dy(double x1, double x2)
        {
            return 100 * (Math.Pow(x1, 2) - x2);
        }

        double G(double x1, double x2, double alpha)
        {
            return Func(x1 - alpha * Func_Dx(x1, x2), x2 - alpha * Func_Dy(x1, x2));
        }

        public double Start(double x0, double y0, double epsilon)
        {
            DichotomyMethod dich = new DichotomyMethod
            {
                DichotomyFunc = G
            };

            int NMAX = 200;
            int lBound = -10000;
            int rBound = 10000;

            double[] x = new double[NMAX];
            double[] y = new double[NMAX];
            double[] alpha = new double[NMAX];

            //Начальное приближение u[0]
            x[0] = x0;
            y[0] = y0;

            Console.WriteLine($"Результаты: x0: {x[0]} y0: {y[0]}");
            Console.WriteLine($"x (k+1) \t\t|\t y (k+1) \t\t|\t f (x(k+1), y(k+1))");

            int k;
            for (k = 0; ; k++)
            {
                alpha[k] = dich.Start(lBound, rBound, x[k], y[k], epsilon).X;

                x[k + 1] = x[k] - alpha[k] * Func_Dx(x[k], y[k]);
                y[k + 1] = y[k] - alpha[k] * Func_Dy(x[k], y[k]);

                Console.WriteLine($"{Math.Round(x[k + 1],6)} \t|\t {Math.Round(y[k + 1],6)} \t|\t {Math.Round(Func(x[k + 1], y[k + 1]), 6)}");

                if (k > 1)
                {
                    if (Math.Abs(x[k + 1] - x[k]) < epsilon)
                    {
                        break;
                    }
                }
            }

            Console.WriteLine($"Точка минимума:");
            Console.WriteLine($"f ({x[k + 1]}, {y[k + 1]}) = {Func(x[k + 1], y[k + 1])}");

            return Func(x[k + 1], y[k + 1]);
        }
    }
}
