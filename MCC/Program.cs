﻿using System;

namespace MCC
{
    class Program
    {
        static void Main(string[] args)
        {
            // Lab 03

            double x = 5;
            double y = 5;
            double epsilon = 0.001;

            GreatDescent desc = new GreatDescent();
            desc.Start(x, y, epsilon);

            Console.ReadKey();
        }       
    }
}
