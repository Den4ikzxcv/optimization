﻿using System;
using System.Linq;

namespace QuadraticInterpolation
{
    public class Interpolation
    {
        double Function(double x)
        {
            var res = -3 * Math.Pow(x, 3) + Math.Pow(x, 2) - (1 / x);
            return Math.Round(res, 8);
        }

        double FunctionTest(double x)
        {
            var res = 2 * Math.Pow(x, 2) + 16 / x;
            return Math.Round(res, 8);
        }

        FuncMin GetMin(FuncMin[] items)
        {
            var Fmin = items.Min(x => x.F);
            return items.First(n => n.F == Fmin);
        }

        double GetMin(double[] items)
        {
            return items.Min();
        }

        double GetStationaryPoint(double x1, double x2, double x3, double f1, double f2, double f3)
        {
            var a1 = (f2 - f1) / (x2 - x1);
            var a2 = (1 / (x3 - x2)) * (((f3 - f1) / (x3 - x1)) - ((f2 - f1) / (x2 - x1)));

            return -1 * (a1 / (2 * a2)) + ((x1 + x2) / 2);
        }

        FuncMin[] CreateMinArr(double x1, double x2, double x3, double f1, double f2, double f3)
        {
            return new FuncMin[]
            {
                new FuncMin { X = x1, F = f1 },
                new FuncMin { X = x2, F = f2 },
                new FuncMin { X = x3, F = f3 }
            };
        }

        public void Start(double leftBound, double rightBound, double epsilon = 0.003, double step = 0.001)
        {
            double x1 = leftBound;
            double x2 = x1 + step;
            double x3 = 0;

            double xSt = 0;
            double fSt = 0;

            Console.WriteLine($"x1\t\tx2\t\tx3\t\tf1\t\tf2\t\tf3\t\txSt\t\tfSt\t\t");
            bool firstIteration = true;

            while (true)
            {
                var f1 = Function(x1);
                var f2 = Function(x2);
                if (firstIteration)
                {
                    if (f1 > f2)
                    {
                        x3 = x1 + 2 * step;
                    }
                    else
                    {
                        x3 = x1 - 2 * step; //TODO: chekc mb we don't need 2 here
                    }
                    
                    if (x3 < leftBound || x3 > rightBound)
                    {
                        x3 = x1 + 2 * step;
                    }
                }

                var f3 = Function(x3);

                var min = GetMin(CreateMinArr(x1, x2, x3, f1, f2, f3));
                var Fmin = min.F;
                var Xmin = min.X;

                xSt = GetStationaryPoint(x1, x2, x3, f1, f2, f3);

                // Проверить принадлежит ли нашему участку
                if((xSt < x1 || xSt > x3) && firstIteration)
                {
                    xSt = x1;
                }

                fSt = Function(xSt);
                Console.WriteLine($"{Math.Round(x1, 4)}\t\t{Math.Round(x2, 4)}\t\t{Math.Round(x3, 4)}\t\t" +
                    $"{Math.Round(f1, 4)}\t\t{Math.Round(f2, 4)}\t\t{Math.Round(f3, 4)}\t\t" +
                    $"{Math.Round(xSt, 4)}\t\t{Math.Round(fSt, 4)}\n");

                if (Math.Abs((Fmin - fSt) / fSt) < epsilon
                    && Math.Abs((Xmin - xSt) / xSt) < epsilon * 10)
                {
                    break;
                }

                x2 = GetMin(new double[] { xSt, Xmin });
                x1 = x2 - step;
                x3 = x2 + step;
                firstIteration = false;
            }

            Console.WriteLine($"Стационарная точка минимума: {xSt}");
            Console.WriteLine($"Функция в точке: {fSt}");            
        }
    }


    public class FuncMin
    {
        public double X { get; set; }

        public double F { get; set; }
    }
}
