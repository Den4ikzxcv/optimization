﻿using System;

namespace Newton
{
    public class Newton
    {
        // Функция Розенброка
        double Func(double x1, double x2)
        {
            return Math.Pow((1 - x1), 2) + 100 * Math.Pow((x2 - Math.Pow(x1, 2)), 2);
        }

        static double FuncGradientX1(double x1, double x2)
        {
            return 200 * (x1 - Math.Pow(x2, 2));
        }

        static double FuncGradientX2(double x1, double x2)
        {
            return -800 * (x1 - Math.Pow(x2, 2)) * x2 - 2 * (1 - x2);
        }

        public (double, double) Start(double x1, double x2)
        {
            double x1Step = double.MaxValue;
            double x2Step = double.MaxValue;

            while (NeedContinue(x1Step, x2Step))
            {
                var inversedMatrix = InverseMatrix(FuncJacobian(x1, x2));
                var gradient = new[]
                {
                    FuncGradientX1(x1, x2),
                    FuncGradientX2(x1, x2)
                };

                x1Step = -1 * (inversedMatrix[0, 0] * gradient[0] + inversedMatrix[0, 1] * gradient[1]);
                x2Step = -1 * (inversedMatrix[1, 0] * gradient[0] + inversedMatrix[1, 1] * gradient[1]);

                Console.WriteLine($"Step x1: {Math.Round(x1Step, 8)}\t\t|\t\tStep x2: {Math.Round(x2Step, 8)}");

                x1 += x1Step;
                x2 += x2Step;
            }

            return (x1, x2);
        }

        bool NeedContinue(double x1Step, double x2Step)
        {
            double epsilon = 10e-7;

            return Math.Abs(x1Step) > epsilon || Math.Abs(x2Step) > epsilon;
        }

        double[,] InverseMatrix(double[,] items)
        {
            double det = items[0, 0] * items[1, 1] - items[0, 1] * items[1, 0];
            if (det == 0.0)
            {
                return null;
            }

            return new double[,]
            {
                {  items[1,1] / det, -items[0,1] / det },
                { -items[1,0] / det,  items[0,0] / det }
            };
        }

        double[,] FuncJacobian(double x1, double x2)
        {
            double[,] jacobian = new double[2, 2];
            jacobian[0, 0] = 200;
            jacobian[0, 1] = -400 * x2;
            jacobian[1, 0] = -800 * x2;
            jacobian[1, 1] = -800 * (x1 - 3 * Math.Pow(x2, 2)) + 2;

            return jacobian;
        }
    }
}
