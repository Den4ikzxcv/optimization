﻿using System;

namespace Newton
{
    class Program
    {
        static void Main(string[] args)
        {
            Newton nwt = new Newton();
            var res = nwt.Start(8, 9);

            Console.WriteLine($"Optimization result:\nx1: {res.Item1} | x2: {res.Item2}");

            Console.ReadKey();
        }
    }
}
