﻿using System;

namespace GoldRatio
{
    public class Gold
    {
        public void Start()
        {
            Console.WriteLine($"Calc for function:\n\nf(x) = x^4 + 2x^2 + 4x + 1 = 0 on [-1;0]\n");
            var a = -1d; // left border
            var b = 0d;  // right border
            var epsilon = 0.1;
            var t = 0.618;

            var a1 = a;
            var b1 = b;
            Console.WriteLine("N\tAn\tBn\tBn-An\tLn\tMn\tF(Ln)\tF(Mn)\n");
            for (int i = 0; ; i++)
            {
                var l1 = a1 + (1 - t) * (b1 - a1);
                var m1 = a1 + t * (b1 - a1);

                var fxL1 = CalcFunction(l1);
                var fxM1 = CalcFunction(m1);

                Console.WriteLine($"{i + 1}\t{Math.Round(a1, 4)}\t{Math.Round(b1, 4)}\t{Math.Round(b1 - a1, 4)}" +
                    $"\t{Math.Round(l1, 4)}\t{Math.Round(m1, 4)}\t{Math.Round(fxL1, 4)}\t{Math.Round(fxM1, 4)}");
                Console.WriteLine();
                if (b1 - a1 < epsilon)
                {
                    break;
                }

                if (fxL1 > fxM1)
                {
                    a1 = l1;
                    //b1 = b;
                    l1 = m1;
                }
                else
                {
                    //a1 = a1;
                    b1 = m1;
                    m1 = l1;
                }
            }
            var center = (b1 + a1) / 2;
            Console.WriteLine($"Center of the interval, x = {Math.Round(center, 8)}\n" +
                $"F(x) = {Math.Round(CalcFunction(center), 8)}");
        }

        double CalcFunction(double x)
        {
            var fx = Math.Pow(x, 4) + 2 * Math.Pow(x, 2) + 4 * x + 1;

            return fx;
        }
    }
}
