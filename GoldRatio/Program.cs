﻿using System;

namespace GoldRatio
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Оптимизация унимодальных функции методом Золотого сечения\n");

            new Gold().Start();

            Console.WriteLine("\nРазработал Пасько Денис | А-0В");
            Console.ReadKey();
        }
    }
}
