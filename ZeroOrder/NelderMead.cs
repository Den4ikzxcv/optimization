﻿using System;
using System.Linq;

namespace ZeroOrder
{
    public class NelderMead
    {
        private const int Np = 2; // NP - число аргументов функции
        private readonly double[] _fn = new double[Np + 1];
        private readonly double[,] _simplex = new double[Np, Np + 1]; // NP + 1 - число вершин симплекса

        // Функция Розенброка
        private double Func(double x1, double x2)
        {
            return Math.Pow(1 - x1, 2) + 100 * Math.Pow(x2 - Math.Pow(x1, 2), 2);
        }

        public void Start()
        {
            double[] x = { -2.048, 2.048 }; // Первая вершина начального симплекса (начальная точка)
            const double l = 0.4;
            const double lThresh = 1.0e-5;
            const double cR = 1.0;
            const double alpha = 2.0;
            const double beta = 0.5;
            const double gamma = 0.5;

            Console.WriteLine("Начальная точка:"); // Результат
            for (var i = 0; i < Np; i++)
            {
                Console.WriteLine(x[i]);
            }

            Start(ref x, Np, l, lThresh, cR, alpha, beta, gamma);

            Console.WriteLine("Результат:");
            for (var i = 0; i < Np; i++)
            {
                Console.WriteLine(x[i]);
            }

            Console.WriteLine("Функция в вершинах симплекса:");
            for (var i = 0; i < Np + 1; i++)
            {
                Console.WriteLine(_fn[i]);
            }
        }


        // Создает из точки X регулярный симплекс с длиной ребра L и с NP + 1 вершиной
        // Формирует массив FN значений оптимизируемой функции F в вершинах симплекса
        private void MakeSimplex(double[] x, double l, int np, bool first)
        {
            int i, j;
            var qn = Math.Sqrt(1.0 + np) - 1.0;
            var q2 = l / Math.Sqrt(2.0) * np;
            var r1 = q2 * (qn + np);
            var r2 = q2 * qn;

            for (i = 0; i < np; i++)
            {
                _simplex[i, 0] = x[i];
            }

            for (i = 1; i < np + 1; i++)
            {
                for (j = 0; j < np; j++)
                {
                    _simplex[j, i] = x[j] + r2;
                }
            }

            for (i = 1; i < np + 1; i++)
            {
                _simplex[i - 1, i] = _simplex[i - 1, i] - r2 + r1;
            }

            for (i = 0; i < np + 1; i++)
            {
                for (j = 0; j < np; j++) x[j] = _simplex[j, i];
                _fn[i] = Func(x[0], x[1]); // Значения функции в вершинах начального симплекса
            }

            if (!first)
            {
                return;
            }
            
            Console.WriteLine("Значения функции в вершинах начального симплекса:");
            for (i = 0; i < np + 1; i++)
            {
                Console.WriteLine(_fn[i]);
            }
        }

        // Центр тяжести симплекса
        private double[] CenterOfOfGravity(int k, int np)
        {
            int i;
            var xc = new double[np];
            for (i = 0; i < np; i++)
            {
                double s = 0;
                int j;
                for (j = 0; j < np + 1; j++)
                {
                    s += _simplex[i, j];
                }
                xc[i] = s;
            }

            for (i = 0; i < np; i++)
            {
                xc[i] = (xc[i] - _simplex[i, k]) / np;
            }
            return xc;
        }

        // Отражение вершины с номером k относительно центра тяжести
        // cR – коэффициент отражения
        private void Reflection(int k, double cR, int np)
        {
            var xc = CenterOfOfGravity(k, np); 
            for (var i = 0; i < np; i++)
            {
                _simplex[i, k] = (1.0 + cR) * xc[i] - _simplex[i, k];
            }
        }

        // Редукция симплекса к вершине k
        // gamma – коэффициент редукции
        private void Reduction(int k, double gamma, int np)
        {
            int i, j; 
            var xk = new double[np];

            for (i = 0; i < np; i++)
            {
                xk[i] = _simplex[i, k];
            }

            for (j = 0; j < np; j++)
            {
                for (i = 0; i < np; i++)
                {
                    _simplex[i, j] = xk[i] + gamma * (_simplex[i, j] - xk[i]);
                }
            }

            for (i = 0; i < np; i++)
            {
                _simplex[i, k] = xk[i]; // Восстанавливаем симплекс в вершине k
            }
        }

        // Сжатие/растяжение симплекса.
        // alpha_beta – коэффициент растяжения/сжатия
        private void ShrinkingExpansion(int k, double alphaBeta, int np)
        {
            var xc = CenterOfOfGravity(k, np);
            for (var i = 0; i < np; i++)
            {
                _simplex[i, k] = xc[i] + alphaBeta * (_simplex[i, k] - xc[i]);
            }
        }

        // Длиина ребра симплекса
        private double FindL(double[] x2, int np) 
        {
            double l = 0;
            for (var i = 0; i < np; i++)
            {
                l += x2[i] * x2[i];
            }
            
            return Math.Sqrt(l);
        }

        private (double, int) GetMinAndIndex(double[] arr)
        {
            var item = arr.Min();
            var index = Array.IndexOf(arr, item);

            return (item, index);
        }

        private (double, int) GetMaxAndIndex(double[] arr)
        {
            var item = arr.Max();
            var index = Array.IndexOf(arr, item);

            return (item, index);
        }

        // Восстанавление симплекса
        private void SimplexRestore(int np) 
        {
            int i, imi = -1, imi2 = -1;
            double fmi, fmi2 = double.MaxValue;
            double[] x = new double[np], x2 = new double[np];
            
            (fmi, imi) = GetMinAndIndex(_fn); 
            
            for (i = 0; i < np + 1; i++)
            {
                var f = _fn[i];
                if (f != fmi && f < fmi2)
                {
                    fmi2 = f;
                    imi2 = i;
                }
            }

            for (i = 0; i < np; i++)
            {
                x[i] = _simplex[i, imi];
                x2[i] = _simplex[i, imi] - _simplex[i, imi2];
            }

            MakeSimplex(x, FindL(x2, np), np, false);
        }

        // Возвращает true, если длина хотя бы одного ребра симплекса превышает lThresh,
        // или false - в противном случае
        private bool NotStopYet(double lThresh, int np)
        {
            int i;
            double[] x = new double[np], x2 = new double[np];
            for (i = 0; i < np; i++)
            {
                int j;
                for (j = 0; j < np; j++)
                {
                    x[j] = _simplex[j, i];
                }
                
                for (j = i + 1; j < np + 1; j++)
                {
                    int k;
                    for (k = 0; k < np; k++)
                    {
                        x2[k] = x[k] - _simplex[k, j];
                    }
                    
                    if (FindL(x2, np) > lThresh) 
                        return true;
                }
            }

            return false;
        }

        // Выполняет поиск экстремума (минимума) функции
        private void Start(
            ref double[] x,
            int np,
            double l,
            double lThresh,
            double cR,
            double alpha,
            double beta,
            double gamma)
        {
            int j = 0, kr = 0; // Предельное число шагов алгоритма
            const int jMx = 10000; // Предельное число шагов алгоритма
            var x2 = new double[np];
            var xR = new double[np];
            // число шагов алгоритма, после выполнения которых симплекс восстанавливается
            const int krTodo = 60;

            Console.WriteLine("L = " + l);
            Console.WriteLine("L_thres = " + lThresh);
            Console.WriteLine("cR = " + cR);
            Console.WriteLine("alpha = " + alpha);
            Console.WriteLine("beta = " + beta);
            Console.WriteLine("gamma = " + gamma);

            MakeSimplex(x, l, np, true);

            while (NotStopYet(lThresh, np) && j < jMx)
            {
                j++; // Число итераций
                kr++;
                if (kr == krTodo)
                {
                    kr = 0;
                    SimplexRestore(np); // Восстановление симплекса
                }

                double fMin;
                (fMin, _) = GetMinAndIndex(_fn); 

                var ima = -1;// Номер отражаемой вершины
                double fMax;
                (fMax, ima) = GetMaxAndIndex(_fn);
                
                int i;
                for (i = 0; i < np; i++)
                {
                    x[i] = _simplex[i, ima];
                }

                Reflection(ima, cR, np);
                for (i = 0; i < np; i++)
                {
                    xR[i] = _simplex[i, ima];
                }

                var fR = Func(xR[0], xR[0]);
                int j2;
                if (fR > fMax)
                {
                    ShrinkingExpansion(ima, beta, np);

                    for (i = 0; i < np; i++)
                    {
                        x2[i] = _simplex[i, ima];
                    }

                    var fS = Func(x2[0], x2[1]);

                    if (fS > fMax)
                    {
                        for (i = 0; i < np; i++)
                        {
                            _simplex[i, ima] = x[i];
                        }

                        Reduction(ima, gamma, np);

                        for (i = 0; i < np + 1; i++)
                        {
                            if (i == ima) continue;
                            
                            for (j2 = 0; j2 < np; j2++)
                            {
                                x2[j2] = _simplex[j2, i];
                            }
                            // Значения функций в вершинах симплекса после редукции.
                            // В вершине ima значение функции сохраняется
                            _fn[i] = Func(x2[0], x2[1]);
                        }
                    }
                    else
                    {
                        _fn[ima] = fS;
                    }
                }
                else if (fR < fMin)
                {
                    ShrinkingExpansion(ima, alpha, np);
                    
                    for (j2 = 0; j2 < np; j2++) x2[j2] = _simplex[j2, ima];
                    var fE = Func(x2[0], x2[1]);
                    if (fE > fMin)
                    {
                        for (j2 = 0; j2 < np; j2++) _simplex[j2, ima] = xR[j2];
                        _fn[ima] = fR;
                    }
                    else
                    {
                        _fn[ima] = fE;
                    }
                }
                else
                {
                    _fn[ima] = fR;
                }
            }

            Console.WriteLine("Число итераций: " + j);
        }
    }
}