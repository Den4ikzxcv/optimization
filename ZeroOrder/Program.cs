﻿using System;

namespace ZeroOrder
{
    class Program
    {
        static void Main(string[] args)
        {
            HookeJeeves hk = new();
          //  hk.Start(8,9);

            NelderMead nm = new();
            nm.Start();

            Console.ReadKey();
        }
    }
}
