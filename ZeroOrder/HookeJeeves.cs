﻿using System;

namespace ZeroOrder
{
    public class HookeJeeves
    {
        // Функция Розенброка
        double Func(double x1, double x2)
        {
            return Math.Pow((1 - x1), 2) + 100 * Math.Pow((x2 - Math.Pow(x1, 2)), 2);
        }

        double FuncTest(double x1, double x2)
        {
            return 4 * Math.Pow((x1 - 5), 2) + Math.Pow((x2 - 6), 2);
        }

        const int n = 2;
        int k = 0;
        double d1 = 1;
        double d2 = 2;
        double epsilon = 0.0001;
        
        public void Start(double x1, double x2)
        {
            var x0 = Func(x1, x2);
            var found1 = false;
            var found2 = false;
            double xd1 = x1;
            double xd2 = x2;

            Console.WriteLine($"x0: ({x1}, {x2})");
            Console.WriteLine($"f(x0): {x0}");

            Console.WriteLine($"  \tx\t\ty\t\tf(x,y)\n");
            for (; ;)
            {
                // 2
                var j = 0;
                while (true)
                {
                    (xd1, found1) = ConfigurePoint1(x0, d1, xd1, xd2);
                    //3
                    if (NeedResearch(ref d1, found1, j))
                    {
                        if (NeedExit(d1))
                        {
                            break;
                        }
                        j++;
                        continue;
                    }
                    break;
                };
                x0 = Func(xd1, x2);

                j = 0;
                while (true)
                {
                    (xd2, found2) = ConfigurePoint2(x0, d2, xd1, xd2);
                    //3
                    if (NeedResearch(ref d2, found2, j))
                    {
                        if (NeedExit(d2))
                        {
                            break;
                        }
                        j++;
                        continue;
                    }
                    break;
                }

                // 4
                Console.WriteLine($" i:\t{Math.Round(xd1, 4)}\t\t{Math.Round(xd2, 4)}\t\tf={Math.Round(Func(xd1, xd2), 4)}");

                var innerSum = SubPoints(xd1, xd2, x1, x2);

                (x1, x2) = SumPoints(xd1, xd2, innerSum.Item1, innerSum.Item2);
                x0 = Func(x1, x2);
                Console.WriteLine($"x0:\t{Math.Round(xd1, 4)}\t\t{Math.Round(xd2, 4)}\t\tf={Math.Round(x0, 4)}\n");

                // 5
                if (NeedExitBoth())
                    break;
            }

            Console.WriteLine($"Min: ({x1}, {x2})");
            Console.WriteLine($"f(min): {x0}");
        }

        (double, double) SumPoints(double x1, double y1, double x2, double y2)
        {
            return (x1 + x2, y1 + y2);
        }

        (double, double) SubPoints(double x1, double y1, double x2, double y2)
        {
            return (x1 - x2, y1 - y2);
        }

        (double, bool) ConfigurePoint1(double x0, double d, double xd1, double xd2)
        {
            var xd1Val = Func(xd1 + d, xd2);
            if (IsLowerValue())
            {
                return (xd1 + d, true);
            }

            xd1Val = Func(xd1 - d, xd2);
            
            return (xd1 - d, IsLowerValue());

            bool IsLowerValue()
            {
                return xd1Val < x0;
            }
        }

        (double, bool) ConfigurePoint2(double x0, double d, double xd1, double xd2)
        {
            var xd2Val = Func(xd1, xd2 + d);
            if (IsLowerValue())
            {
                return (xd2 + d, true);
            }

            xd2Val = Func(xd1, xd2 - d);

            return (xd2 - d, IsLowerValue());

            bool IsLowerValue()
            {
                return xd2Val < x0;
            }
        }

        bool NeedExit(double d)
        {
            if (d < epsilon)
            {
                return true;
            }

            return false;
        }

        bool NeedResearch(ref double d, bool found, int j)
        {
            if (found)
            {
                return false;
            }

            if (j < n)
            {
                return true;
            }

            d /= 2; //TODO: mb choose better value            
            return true;
        }

        bool NeedExitBoth()
        {
            var exit1 = NeedExit(d1);
            var exit2 = NeedExit(d2);
            if (!exit1)
            {
                d1 /= 2;
            }

            if (!exit2)
            {
                d2 /= 2;
            }

            return exit1 && exit2;
        }
    }
}
