﻿using System;

namespace Dichotomy
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Оптимизация унимодальных функции методом дихотомии\n");
            DichotomyMethod count = new DichotomyMethod(-2, 0);
            count.Start(30);

            Console.WriteLine("\nРазработал: Пасько Денис | А-0В");
            Console.ReadKey();
        }
    }
}
