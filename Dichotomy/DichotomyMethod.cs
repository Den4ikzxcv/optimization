﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Dichotomy
{
    class DichotomyMethod
    {
        private double _boundLeft = -2;
        private double _boundRight = 0;


        public DichotomyMethod(double boundLeft, double boundRight)
        {
            _boundLeft = boundLeft;
            _boundRight = boundRight;
        }

        private string func12 = "f(x) = -3x^3 + x^2 - 1/x [-2 ; 0]";
        // [-2 ; 0]
        double CountFunction12(double x)
        {
            var res = -3 * Math.Pow(x, 3) + Math.Pow(x, 2) - (1 / x);
            return Math.Round(res, 3);
        }

        private string func11 = "f(x) = x / (3-x) - 2x + 2 = 0 [0 ; 2]";
        //[0 ; 2]
        double CountFunction11(double x)
        {
            var res = x / (3 - x) - 2 * x + 2;
            return Math.Round(res, 3);
        }

        public Results Start(int N = 8)
        {
            //Аналитическое
            //var roots = new[] {1.5, 2};
            //Получить корни в ниших границах
            //var rootsInBound = FindRootsInBounds(roots);
            var iterations = N / 2;
            var epsilon = 0.1;

            double x1 = 0.0;
            double x2 = 0.0;
            double f1 = 0.0;
            double f2 = 0.0;

            Console.WriteLine($"Итераций: {iterations}\n");
            Console.WriteLine($"Функция: {func12}\n");
            Console.WriteLine($"#\tX1\tX2\t" +
                              $"f1\t \tf2\t" +
                              $"a\t b");
            Console.WriteLine($"---------------------------------------------------------------");
            List<Results> x1Res = new List<Results>();
            List<Results> x2Res = new List<Results>();
            for (int i = 0; i < iterations; i++)
            {
                x1 = (1.0 / 2.0) * (_boundLeft + _boundRight) - (epsilon / 2);
                x2 = (1.0 / 2.0) * (_boundLeft + _boundRight) + (epsilon / 2);

                f1 = CountFunction12(x1);
                f2 = CountFunction12(x2);

                if (f1 <= f2)
                {
                    // boundLeft = boundLeft;
                    _boundRight = x2;
                    Console.WriteLine($"{i}\t{Math.Round(x1, 3)}\t{Math.Round(x2, 3)}\t" +
                                      $"{Math.Round(f1, 3)}\t<=\t{Math.Round(f2, 3)}\t" +
                                      $"{Math.Round(_boundLeft, 3)}\t {Math.Round(_boundRight, 3)}\t");
                }
                else
                {
                    _boundLeft = x1;
                    // boundRight = boundRight;
                    Console.WriteLine($"{i}\t{Math.Round(x1, 3)}\t{Math.Round(x2, 3)}\t" +
                                      $"{Math.Round(f1, 3)}\t>\t{Math.Round(f2, 3)}\t" +
                                      $"{Math.Round(_boundLeft, 3)}\t {Math.Round(_boundRight, 3)}\t");
                }
                x1Res.Add(new Results { Iteration = i, X = x1, F = f1 });
                x2Res.Add(new Results { Iteration = i, X = x2, F = f2 });
            }

            var root = FindLowestRoot(x1Res, x2Res);
            Console.WriteLine($"\nОтвет: x*({root.Iteration}) = {Math.Round(root.X, 3)} , f*({root.Iteration}) = {Math.Round(root.F, 3)}");

            return root;
        }

        Results FindLowestRoot(List<Results> x1Res, List<Results> x2Res)
        {
            var min1 = x1Res.Min();
            var min2 = x2Res.Min();

            if (min1.F > min2.F)
                return min1;

            return min2;
        }

        double[] FindRootsInBounds(double[] roots)
        {
            List<double> res = new List<double>();
            foreach (var root in roots)
            {
                if (root >= _boundLeft & root <= _boundRight)
                    res.Add(root);
            }

            return res.ToArray();
        }

        /// <summary> Нахождение корней по теореме Виета-Кардана </summary>
        /// <param name="a">число при х^3</param>
        /// <param name="b">число при х^2</param>
        /// <param name="c">число при х</param>
        /// <returns></returns>
        double[] FindRoots(double a, double b, double c)
        {
            double Q = (Math.Pow(a, 2) - 3 * b) / 9;

            double R = ((2 * Math.Pow(a, 3)) - 9 * a * b + 27 * c) / 54;

            double S = Math.Pow(Q, 3) - Math.Pow(R, 2);

            if (S < 0) return null;

            //double x1 = 2 * Math.Sqrt(Q) * Math.Cos();

            return null;
        }

    }

    public class Results : IComparable
    {
        public int Iteration { get; set; }
        public double X { get; set; }
        public double F { get; set; }

        public int CompareTo(object obj)
        {
            var compObj = obj as Results;

            if (this.F < compObj.F) 
            { 
                return -1;
            }

            if (Math.Round(this.F, 10) == Math.Round(compObj.F, 10)) 
            { 
                return 0;
            }

            return 1;
        }
    }
}
