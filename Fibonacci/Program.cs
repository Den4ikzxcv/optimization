﻿using System;

namespace Fibonacci
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Оптимизация унимодальных функции методом числе Фибоначчи\n");
            new Fibonacci().Start();

            Console.WriteLine("\nРазработал Пасько Денис | А-0В");
            Console.ReadKey();
        }
    }
}
