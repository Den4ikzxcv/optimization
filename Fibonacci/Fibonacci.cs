﻿using System;

namespace Fibonacci
{
    public class Fibonacci
    {
        public void Start()
        {
            Console.WriteLine($"Calc for function:\n\nf(x) = 4.2 * x^2 + 23/x = 0 on [0;8]\n");

            var a = 0d;
            var b = 8d;
            //int n = GetN(a, b); // Or calculate N programmatically
            int n = 6;

            Console.WriteLine("N\tAn\tBn\tBn-An\tAn+i*c\tAn+k*c\tF(Ln)\tF(Mn)\n");
            for (int i = 0; i < n; i++)
            {
                var l = CalcL(n, i, a, b);
                var m = CalcM(n, i, a, b);

                Console.WriteLine($"{i + 1}\t{Math.Round(a, 4)}\t{Math.Round(b, 4)}\t{Math.Round(b - a, 4)}" +
                  $"\t{Math.Round(0d, 4)}\t{Math.Round(0d, 4)}\t{Math.Round(CalcFunction(l), 4)}\t{Math.Round(CalcFunction(m), 4)}");
                Console.WriteLine();
                if (l < m)
                {
                    // a = a;
                    b = m;
                }
                else
                {
                    a = l;
                    // b = b;
                }
            }

            var xMin = (a + b) / 2;
            Console.WriteLine($"Xmin = {Math.Round(xMin, 8)}\n" +
             $"F(Xmin) = {Math.Round(CalcFunction(xMin), 8)}");
        }

        double CalcL(int n, int i, double a, double b)
        {
            return a + (GetFibonacciNumber(n - i - 1) / GetFibonacciNumber(n - i + 1)) * (b - a);
        }

        double CalcM(int n, int i, double a, double b)
        {
            return a + (GetFibonacciNumber(n - i) / GetFibonacciNumber(n - i + 1)) * (b - a);
        }

        double GetFibonacciNumber(int n)
        {
            var res = 1;
            int prev = 0;
            for (int i = 1; i < n; i++)
            {
                res = res + prev;
                prev = res - prev;
            }

            return res;
        }

        double CalcFunction(double x)
        {
            var fx = 4.2 * Math.Pow(x, 2) + 23 / x;

            return fx;
        }

        int GetN(double a, double b)
        {
            var epsilon = 0.001;
            for (int i = 0; ; i++)
            {
                var res = (b - a) / (2 * epsilon);
                if (GetFibonacciNumber(i + 1) >= res)
                {
                    return i + 1;
                }
            }
        }
    }
}
